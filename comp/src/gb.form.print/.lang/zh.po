#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form.print 3.15.90\n"
"POT-Creation-Date: 2020-11-18 18:34 UTC\n"
"PO-Revision-Date: 2020-11-18 18:34 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Generic preview and print form"
msgstr "通用预览和打印窗体"

#: FPreview.class:305
msgid "PDF files"
msgstr "PDF文件"

#: FPreview.class:305
msgid "Postscript files"
msgstr "Postscript文件"

#: FPreview.class:365 FPrinting.form:30
msgid "Cancel"
msgstr "取消"

#: FPreview.class:365
msgid "Replace"
msgstr "替换"

#: FPreview.class:365
msgid ""
"This file already exists.\n"
"Do you want to replace it?"
msgstr ""
"此文件已存在。\n"
"是否替换它？"

#: FPreview.form:79
msgid "Print preview"
msgstr "打印预览"

#: FPreview.form:87
msgid "Print to file"
msgstr "打印到文件"

#: FPreview.form:106
msgid "Toggle option panel"
msgstr "切换选项面板"

#: FPreview.form:118
msgid "Print"
msgstr "打印"

#: FPreview.form:132
msgid "Zoom out"
msgstr "缩小"

#: FPreview.form:138
msgid "100 %"
msgstr ""

#: FPreview.form:144
msgid "Zoom in"
msgstr "放大"

#: FPreview.form:152
msgid "One page"
msgstr "单页"

#: FPreview.form:160
msgid "Two pages"
msgstr "双页"

#: FPreview.form:168
msgid "Full width"
msgstr "全宽"

#: FPreview.form:176
msgid "Real size"
msgstr "实际尺寸"

#: FPreview.form:208
msgid "Resolution"
msgstr "分辨率"

#: FPreview.form:220
msgid "DPI"
msgstr ""

#: FPreview.form:230
msgid "Two-sided"
msgstr "双面"

#: FPreview.form:237
msgid "Long side"
msgstr "长边"

#: FPreview.form:237
msgid "None"
msgstr "无"

#: FPreview.form:237
msgid "Short side"
msgstr "短边"

#: FPreview.form:247
msgid "Copies"
msgstr "份数"

#: FPreview.form:263
msgid "Orientation"
msgstr "方向"

#: FPreview.form:270
msgid "Landscape"
msgstr "横向"

#: FPreview.form:270
msgid "Portrait"
msgstr "纵向"

#: FPreview.form:280
msgid "Paper"
msgstr "纸张"

#: FPreview.form:301
msgid "Width"
msgstr "宽度"

#: FPreview.form:311
msgid "mm"
msgstr ""

#: FPreview.form:321
msgid "Height"
msgstr "高度"

#: FPreview.form:338
msgid "Grayscale"
msgstr "灰度"

#: FPreview.form:343
msgid "Collate copies"
msgstr "逐份打印"

#: FPreview.form:348
msgid "Reverse order"
msgstr "倒序"

#: FPreview.form:356
msgid "Margins"
msgstr "页边距"

#: FPreview.form:365
msgid "Left"
msgstr "左边"

#: FPreview.form:386
msgid "Right"
msgstr "右边"

#: FPreview.form:407
msgid "Top"
msgstr "顶边"

#: FPreview.form:428
msgid "Bottom"
msgstr "底边"

#: FPrinting.form:11
msgid "Printing"
msgstr "打印"
