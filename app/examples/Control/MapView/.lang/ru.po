# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-28 09:00+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Control/MapView/.project:21
msgid "Maps"
msgstr "Карты"

#: app/examples/Control/MapView/.project:22
msgid "A simple MapViewer and Map example"
msgstr "Простой просмотрщик карт и пример карты"

#: app/examples/Control/MapView/.src/FMain.class:19 app/examples/Control/MapView/.src/FMain.class:25
msgid "version"
msgstr "версия"

#: app/examples/Control/MapView/.src/FMain.class:21
msgid "contributors"
msgstr "участники"

#: app/examples/Control/MapView/.src/FMain.class:138
msgid ""
"A simple MapViewer and Map example by\n"
"Fabien Bodard (gambix@users.sourceforge.net)\n"
"\n"
"Enjoy it !   :-)"
msgstr ""
"Простой просмотрщик карт и пример карты от\n"
"Фабьен Бодар (gambix@users.sourceforge.net)\n"
"\n"
"Наслаждайтесь ! :-)"

#: app/examples/Control/MapView/.src/FMain.form:29
msgid "Layers"
msgstr "Слои"

#: app/examples/Control/MapView/.src/FMain.form:55
msgid "Lens zoom"
msgstr "Зум объектива"

#: app/examples/Control/MapView/.src/FMain.form:66
msgid "&About..."
msgstr "О программе..."

#: app/examples/Control/MapView/.src/FMain.form:78
msgid ""
"Welcome to this Gambas Map component demo.<br>\n"
"<br>\n"
"Use your mouse to drag the map with the left button and zoom with the wheel button.<br>\n"
"A little extra is here to: use CTRL + mouse left button on the map and see what happens ;-) ."
msgstr ""
"Добро пожаловать в демонстрационную версию компонента карт Gambas.<br>\n"
"<br>\n"
"Используйте мышь, чтобы перетащить карту левой кнопкой и увеличить с помощью колёсика.<br>\n"
"Немного больше здесь: используйте CTRL + левую кнопку мыши на карте и посмотрите, что произойдёт ;-)."

